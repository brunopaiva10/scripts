#!/bin/bash 

for i in {1..5}
do
    echo "========== EXECUTION $i ==========" ; wait
    # setup
    export OMP_NUM_THREADS=32; wait # number of threads
    PREFIX=$HOME/mamute; wait
    BIN_SPHERE_DIR=$PREFIX/bin_sphere; wait
    BIN_SPHERE=$BIN_SPHERE_DIR/modeling; wait
    BIN_DIR=$PREFIX/bin; wait
    BIN=$BIN_DIR/modeling; wait
    PROJ_DIR=$PREFIX/projects/example; wait
    SCRIPT_DIR=$PREFIX/scripts; wait
    CONFIG_FILE=$PROJ_DIR/modeling.txt ; wait
    CONFIG_SOURCE=$PROJ_DIR/rcv_scr.json
    BUILD_SPHERE_DIR=$PREFIX/build_sphere; wait
    BUILD_DIR=$PREFIX/build; wait
    cd $PREFIX && git checkout dev; wait

    # clean up
    rm -rf $BIN_DIR $BIN_SPHERE_DIR $BUILD_DIR $BUILD_SPHERE_DIR $PROJ_DIR $PREFIX/*.bin; wait

    # compilation using sphere method
    mkdir $BUILD_SPHERE_DIR ; wait
    cd $BUILD_SPHERE_DIR ; wait
    cmake $PREFIX -DCMAKE_INSTALL_PREFIX=$PREFIX -DTEST=ON -DVERBOSE=ON -DOPENMP=ON -DSPHERE=ON ; wait
    make -j ; wait
    make -j install ; wait
    cd $PREFIX; wait
    mv $PREFIX/bin $BIN_SPHERE_DIR; wait

    # compilation WITHOUT using sphere method
    mkdir $BUILD_DIR ; wait
    cd $BUILD_DIR ; wait
    cmake $PREFIX -DCMAKE_INSTALL_PREFIX=$PREFIX -DTEST=ON -DVERBOSE=ON -DOPENMP=ON -DSPHERE=OFF ; wait
    make -j ; wait
    make -j install ; wait
    cd $PREFIX; wait

    NX=101
    NY=101
    NZ=101
    cp $PREFIX/projects/quick-start $PROJ_DIR -r ; wait

    rm -rf $PROJ_DIR/modeling.txt ; wait
    NX=101
    NY=101
    NZ=101
    echo "nx=$NX
    ny=$NY
    nz=$NZ
    ns=401
    border=50
    dx=10.0
    dy=10.0
    dz=10.0
    vel=velocity_model.bin
    fpeak=20
    dt=0.0005
    amplitude=1
    n_src=1
    ws_flag=0
    proj_dir=$PROJ_DIR
    stencil=4
    ox=0.0
    oy=0.0
    oz=0.0" > $CONFIG_FILE; wait

    rm -rf $PROJ_DIR/velocity_modeling.json ; wait
    echo "{
        \"type\" : \"constant\",
        \"v\" : 2500
    }" > $PROJ_DIR/velocity_modeling.json ; wait

    RS_VALUES=(
        "500 500 500 500 500 500"  # Esfera completa
        "500 500 500 500 0 1000"    # 1/2 de esfera
        "500 500 0 1000 0 1000"     # 1/4 de esfera
        "0 1000 0 1000 0 1000"      # 1/8 de esfera
    )

    for RS in "${RS_VALUES[@]}"
    do
        echo "========== TEST WITH SPHERE = $RS ==========" ; wait
        # clean up
        cd $PREFIX; wait
        rm -rf $CONFIG_SOURCE $PROJ_DIR/source.bin $PROJ_DIR/velocity_model.bin; wait

        set -- $RS
        x_first=$1
        x_delta=$2
        y_first=$3
        y_delta=$4
        z_first=$5
        z_delta=$6

        z_first_receivers=$(($z_first + 200))

        echo "{
        \"receivers\": {
            \"x\": {
                \"first\": $x_first,
                \"last\": 1000,
                \"delta\": 10000
            },
            \"y\": {
                \"first\": $y_first,
                \"last\": 1000,
                \"delta\": 10000
            },
            \"z\": {
                \"first\": $z_first_receivers,
                \"last\": 1000,
                \"delta\": 10000
            }
        },
        \"sources\": {
            \"x\": {
                \"first\": $x_first,
                \"last\": 1000,
                \"delta\": $x_delta
            },
            \"y\": {
                \"first\": $y_first,
                \"last\": 1000,
                \"delta\": $y_delta
            },
            \"z\": {
                \"first\": $z_first,
                \"last\": 1000,
                \"delta\": $z_delta
            }
        }
    }" > $CONFIG_SOURCE ; wait
        python3 $SCRIPT_DIR/generate_data/source.py $CONFIG_FILE ; wait
        # python3 $SCRIPT_DIR/plot_data/plot_trace.py source.bin ; wait
        python3 $SCRIPT_DIR/generate_data/rcv_scr.py $PROJ_DIR/rcv_scr.json ; wait
        # python3 $SCRIPT_DIR/plot_data/plot_src_rcv.py $PREFIX/src_coord.bin $PREFIX/rcv_coord_0.bin ; wait
        python3 $SCRIPT_DIR/generate_data/velocity.py $PROJ_DIR/velocity_modeling.json $CONFIG_FILE ; wait
        # python3 $SCRIPT_DIR/plot_data/plot_3d.py $NX $NY $NZ $PREFIX/velocity_model.bin ; wait
        mv $PREFIX/*.bin $PROJ_DIR/ ; wait

        echo "========== TEST WITH rs = $RS AND SPHERE=0N ==========" ; wait
        time $BIN_SPHERE $CONFIG_FILE; wait
        # python3 $SCRIPT_DIR/plot_data/plot_trace.py $PROJ_DIR/dobs_0.bin ; wait
        mv "$PROJ_DIR/dobs_0.bin" "$PROJ_DIR/dobs0-esfera-rs${RS// /-}.bin" ; wait

        echo "========== TEST WITH rs = $RS AND SPHERE=OFF ==========" ; wait
        time $BIN $CONFIG_FILE; wait
        # python3 $SCRIPT_DIR/plot_data/plot_trace.py $PROJ_DIR/dobs_0.bin ; wait
        mv "$PROJ_DIR/dobs_0.bin" "$PROJ_DIR/dobs0-not-esfera-rs${RS// /-}.bin" ; wait

        # echo "========== COMPARING RESULTS for rs = $RS ==========" ; wait
        # python3 scripts/mse.py "$PROJ_DIR/dobs0-esfera-rs${RS// /-}.bin" "$PROJ_DIR/dobs0-not-esfera-rs${RS// /-}.bin"
    done ; wait

    echo "========== END OF EXECUTION $i ==========" ; wait
done ; wait
