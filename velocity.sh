#/bin/bash 

for i in {1..5}
do
    echo "========== EXECUTION $i ==========" ; wait

    # setup
    export OMP_NUM_THREADS=32; wait # number of threads
    PREFIX=$HOME/mamute; wait
    BIN_SPHERE_DIR=$PREFIX/bin_sphere; wait
    BIN_SPHERE=$BIN_SPHERE_DIR/modeling; wait
    BIN_DIR=$PREFIX/bin; wait
    BIN=$BIN_DIR/modeling; wait
    PROJ_DIR=$PREFIX/projects/example; wait
    SCRIPT_DIR=$PREFIX/scripts; wait
    CONFIG_FILE=$PROJ_DIR/modeling.txt ; wait
    CONFIG_VELOCITY=$PROJ_DIR/velocity_modeling.json ; wait
    BUILD_SPHERE_DIR=$PREFIX/build_sphere; wait
    BUILD_DIR=$PREFIX/build; wait
    cd $PREFIX && git checkout dev; wait

    # clean up
    rm -rf $BIN_DIR $BIN_SPHERE_DIR $BUILD_DIR $BUILD_SPHERE_DIR $PROJ_DIR $PREFIX/*.bin; wait

    # compilation using sphere method
    mkdir $BUILD_SPHERE_DIR ; wait
    cd $BUILD_SPHERE_DIR ; wait
    cmake $PREFIX -DCMAKE_INSTALL_PREFIX=$PREFIX -DTEST=ON -DVERBOSE=ON -DOPENMP=ON -DSPHERE=ON ; wait
    make -j ; wait
    make -j install ; wait
    cd $PREFIX; wait
    mv $PREFIX/bin $BIN_SPHERE_DIR; wait

    # compilation WITHOUT using sphere method
    mkdir $BUILD_DIR ; wait
    cd $BUILD_DIR ; wait
    cmake $PREFIX -DCMAKE_INSTALL_PREFIX=$PREFIX -DTEST=ON -DVERBOSE=ON -DOPENMP=ON -DSPHERE=OFF ; wait
    make -j ; wait
    make -j install ; wait
    cd $PREFIX; wait

    VEL_VALUES=(1500 2500 3500 4500 5500)
    NX=101
    NY=101
    NZ=101
    cp $PREFIX/projects/quick-start $PROJ_DIR -r ; wait

    rm -rf $PROJ_DIR/modeling.txt ; wait
    NX=101
    NY=101
    NZ=101
    echo "nx=$NX
    ny=$NY
    nz=$NZ
    ns=401
    border=50
    dx=10.0
    dy=10.0
    dz=10.0
    vel=velocity_model.bin
    fpeak=15
    dt=0.0005
    amplitude=1
    n_src=1
    ws_flag=0
    proj_dir=$PROJ_DIR
    stencil=4
    ox=0.0
    oy=0.0
    oz=0.0" > $PROJ_DIR/modeling.txt; wait

    rm -rf $PROJ_DIR/rcv_scr.json ; wait
    echo "{
        \"receivers\": {
            \"x\": {
                \"first\": 500,
                \"last\": 1000,
                \"delta\": 10000
            },
            \"y\": {
                \"first\": 500,
                \"last\": 1000,
                \"delta\": 10000
            },
            \"z\": {
                \"first\": 200,
                \"last\": 1000,
                \"delta\": 10000
            }
        },
        \"sources\": {
            \"x\": {
                \"first\": 500,
                \"last\": 1000,
                \"delta\": 500
            },
            \"y\": {
                \"first\": 500,
                \"last\": 1000,
                \"delta\": 500
            },
            \"z\": {
                \"first\": 0,
                \"last\": 1000,
                \"delta\": 1000
            }
        }
    }" > $PROJ_DIR/rcv_scr.json ; wait
    python3 $SCRIPT_DIR/generate_data/rcv_scr.py $PROJ_DIR/rcv_scr.json ; wait
    # python3 $SCRIPT_DIR/plot_data/plot_src_rcv.py $PREFIX/src_coord.bin $PREFIX/rcv_coord_0.bin ; wait

    for VEL in "${VEL_VALUES[@]}"
    do
        echo "========== TEST WITH VEL = $VEL ==========" ; wait
        cd $PREFIX ; wait
        rm -rf $CONFIG_VELOCITY $PROJ_DIR/source.bin $PROJ_DIR/velocity_model.bin; wait

        echo "{
        \"type\" : \"constant\",
        \"v\" : $VEL
    }" > $CONFIG_VELOCITY ; wait
        python3 $SCRIPT_DIR/generate_data/source.py $CONFIG_FILE ; wait
        # python3 $SCRIPT_DIR/plot_data/plot_trace.py $PREFIX/source.bin ; wait
        python3 $SCRIPT_DIR/generate_data/velocity.py $PROJ_DIR/velocity_modeling.json $CONFIG_FILE ; wait
        # python3 $SCRIPT_DIR/plot_data/plot_3d.py $NX $NY $NZ $PREFIX/velocity_model.bin ; wait
        mv $PREFIX/*.bin $PROJ_DIR/ ; wait

        echo "========== TEST WITH VEL = $VEL AND SPHERE=0N ==========" ; wait
        time $BIN_SPHERE $CONFIG_FILE; wait
        # python3 $SCRIPT_DIR/plot_data/plot_trace.py $PROJ_DIR/dobs_0.bin ; wait
        mv $PROJ_DIR/dobs_0.bin $PROJ_DIR/dobs0-esfera-v$VEL.bin ; wait

        echo "========== TEST WITH VEL = $VEL AND SPHERE=OFF ==========" ; wait
        time $BIN $CONFIG_FILE; wait
        # python3 $SCRIPT_DIR/plot_data/plot_trace.py $PROJ_DIR/dobs_0.bin ; wait
        mv $PROJ_DIR/dobs_0.bin $PROJ_DIR/dobs0-not-esfera-v$VEL.bin ; wait

        # echo "========== COMPARING RESULTS for VEL = $VEL ==========" ; wait
        # python3 scripts/mse.py $PROJ_DIR/dobs0-esfera-v$VEL.bin $PROJ_DIR/dobs0-not-esfera-v$VEL.bin
    done ; wait

    echo "========== END OF EXECUTION $i ==========" ; wait
done
